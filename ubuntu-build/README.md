Compiling a native install on Ubuntu 16.04LTS
=============================================

For all of the reasons mentioned in the paper, producing scripts to compile
complex scientific software on many platforms is not easy. This folder contains
an attempt at set of scripts to compile a version of FEniCS on Ubuntu 16.04
that as closely as possible matches that in the container. This install was used
to generate the workstation results shown in the paper.

    sudo ./ubuntu-deps.sh 
    ./custom-deps.sh
    ./fenics-update
    source fenics.env.conf
    python -c "import dolfin"

These scripts will also compile a version of PETSc that you can use to build
the HPGMG binary. Again, this method was used to generate the workstation results
in the paper.
