#!/bin/bash
source fenics.env.conf

SWIG_VERSION=3.0.10
MPI4PY_VERSION=2.0.0
PETSC4PY_VERSION=3.7.0
SLEPC4PY_VERSION=3.7.0
SLEPC_VERSION=3.7.0
PETSC_VERSION=3.7.0

cd $FENICS_HOME && \
wget -nc --quiet http://downloads.sourceforge.net/swig/swig-${SWIG_VERSION}.tar.gz && \
tar xf swig-${SWIG_VERSION}.tar.gz && \
cd swig-${SWIG_VERSION} && \
./configure --prefix=$FENICS_HOME && \
make -j4 && \
make install

cd $FENICS_HOME && \
wget -nc http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${PETSC_VERSION}.tar.gz && \
tar -xf petsc-lite-${PETSC_VERSION}.tar.gz && \
cd petsc-${PETSC_VERSION} && \
./configure --COPTFLAGS="-O2" \
            --CXXOPTFLAGS="-O2" \
            --FOPTFLAGS="-O2" \
            --with-blas-lib=/usr/lib/libopenblas.a --with-lapack-lib=/usr/lib/liblapack.a \
            --with-c-support \
            --with-debugging=0 \
            --with-shared-libraries \
            --download-suitesparse \
            --download-scalapack \
            --download-metis \
            --download-parmetis \
            --download-ptscotch \
            --download-hypre \
            --download-mumps \
            --download-blacs \
            --download-spai \
            --download-ml \
            --prefix=$FENICS_HOME && \
make && \
make install

export PETSC_DIR=$FENICS_HOME

cd $FENICS_HOME && \
wget -nc http://www.grycap.upv.es/slepc/download/download.php?filename=slepc-${SLEPC_VERSION}.tar.gz -O slepc-${SLEPC_VERSION}.tar.gz && \
tar -xf slepc-${SLEPC_VERSION}.tar.gz && \
cd slepc-${SLEPC_VERSION} && \
./configure --prefix=$FENICS_HOME && \
make && \
make install

export SLEPC_DIR=$FENICS_HOME

PYTHONUSERBASE=$FENICS_HOME pip install --user --upgrade pip && \ 
PYTHONUSERBASE=$FENICS_HOME pip install --user sympy && \
PYTHONUSERBASE=$FENICS_HOME pip install --user \
https://bitbucket.org/mpi4py/mpi4py/downloads/mpi4py-${MPI4PY_VERSION}.tar.gz && \
PYTHONUSERBASE=$FENICS_HOME pip install --user \
https://bitbucket.org/petsc/petsc4py/downloads/petsc4py-${PETSC4PY_VERSION}.tar.gz && \
PYTHONUSERBASE=$FENICS_HOME pip install --user \
https://bitbucket.org/slepc/slepc4py/downloads/slepc4py-${SLEPC4PY_VERSION}.tar.gz
