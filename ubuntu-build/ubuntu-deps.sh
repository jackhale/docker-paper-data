#!/bin/bash

# dev-env-base
apt-get -qq update
apt-get -y install \
    bison cmake flex g++ gfortran git ipython mpich pkg-config wget \
    libopenblas-dev libboost-filesystem-dev libboost-system-dev \
    libboost-program-options-dev libboost-thread-dev libboost-timer-dev \
    libboost-iostreams-dev libeigen3-dev liblapack-dev \
    libmpich-dev libpcre3-dev libhdf5-mpich-dev libgmp-dev \
    libcln-dev libmpfr-dev

# dev-env
apt-get -y install \
    python-dev python-numpy python-six python-ply python-pytest python-h5py \
    python-urllib3 python-setuptools python-pip python-flufl.lock \
    python-matplotlib
