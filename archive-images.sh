#!/bin/bash
docker save quay.io/fenicsproject/stable:2016.1.0r1 | gzip > quay-io-fenicsproject-stable-2016.1.0r1-31b19792.tar.gz
docker save quay.io/jhale/hpgmg:docker-paper | gzip > quay-io-jhale-hpgmg-docker-paper-382e6756.tar.gz
docker save jupyter/datascience-notebook:54838ed4acb1 | gzip > jupyter-data-science-notebook-54838ed4acb1-9b460b15.tar.gz
