#!/usr/bin/python
from dolfin import *
import sys

# Set number of cores and number of dofs per core here (for P1)
ncores = 1
ndofs = 50000

if len(sys.argv) > 1:
    ncores = int(sys.argv[1])
if len(sys.argv) > 2:
    ndofs = int(sys.argv[2])

print "cores = ", ncores
print "ndofs per core = ", ndofs

# Calculate number of vertices for a UnitCubeMesh(i,j,k) refined n times
def nvert(i,j,k,n):
    nv = (i+1)*(j+1)*(k+1)
    earr = [1, 3, 7]
    for r in range(n):
        ne = earr[0]*(i + j + k) + earr[1]*(i*j + j*k + i*k) + earr[2]*i*j*k
        nv += ne
        earr[0] *= 2
        earr[1] *= 4
        earr[2] *= 8
    return nv

# r = number of refinement levels
r = 1

# Make initial guess
n = ndofs*ncores
i0 = int(pow(float(n)/(8**r), 1./3.) + 0.5)
print "i0 = ", i0

mindiff = 1e20
for i in range(i0 - 10, i0 + 10):
    for j in range(i - 5, i + 5):
        for k in range(i - 5, i + 5):
            diff = abs(nvert(i, j, k, r)-n)
            if (diff < mindiff):
                mindiff = diff
                pos = (i,j,k)

i = pos[0]
j = pos[1]
k = pos[2]

print 'Closest cube mesh = ', pos, nvert(i, j, k, r)/ncores

mesh = UnitCubeMesh(i, j, k)
ndofsk = int(ndofs/1000)
filename = "mesh-%04d-%04dKdofs.xdmf"%(ncores, ndofsk)
print "Writing ", filename, " mesh file"
xdmf = XDMFFile(mesh.mpi_comm(), filename)
xdmf.write(mesh)
