FEniCS on HPC with Shifter
==========================

The example codes in the two directories "cpp" and "python" were used to
generate the timings for the evaluation on Shifter on Edison.
For both codes, a series of meshes was generated using the utility script
"mesh_generator.py". This script takes two arguments, the number of
cores and the number of dofs per core, and generates a suitable cube
mesh which has the number of dofs required.

For Edison, we generated meshes with 300000 dofs per core:

```
./mesh_generator.py 24 300000
./mesh_generator.py 48 300000
./mesh_generator.py 96 300000
./mesh_generator.py 192 300000
```

C++ benchmark
-------------

### Compiling with Shifter

The following commands will load an interactive session to compile the code:
```
module load shifter
shifterimg pull quay.io/fenicsproject/stable:2016.1.0r1
salloc -N 1 -p debug -t 0:30:0 --image=quay.io/fenicsproject/stable:2016.1.0r1
shifter /bin/bash
```
In the interactive session (inside Shifter), the code can be compiled as follows:
```
source /home/fenics/fenics.env.conf
cd shifter/cpp
cmake .
make
```

This build will exclusively use libraries from within the container (including
MPI). In order to run efficiently on Cray hardware, the MPI
library must be replaced with Cray MPI at run time (see below).

### Running with Shifter

To run the code with Shifter, we need the system MPI libraries to be
available at runtime. On a Cray system, they are usually in
`/opt/cray/mpt`. If `/opt/cray` is not available inside shifter (as is
the case on Edison), then the relevant libraries, and all their
dependencies must be copied to a visible location (such as
`$SCRATCH/cray-libs`). For Edison, we need to copy `libmpi.so.12`,
`libxpmem.so.0`, `libugni.so.0`, `libpmi.so.0` and `libudreg.so.0`. It
is also necessary to rename or make a symbolic link to
`libmpich.so.12` from `libmpi.so.12`, as the naming convention is
different between the container and Cray.

Having made a copy of the MPI libraries, the code can be run (on two
nodes) from a submission script as follows:

```
#!/bin/bash
#SBATCH --image=docker:quay.io/fenicsproject:2016.1.0r1
#SBATCH --nodes=2
#SBATCH -p debug
#SBATCH -t 00:15:00

module load shifter
srun -n 48 shifter env LD_LIBRARY_PATH=$SCRATCH/cray-libs ./bench_3d --system=edison-shifter --filename=mesh-0048-0300Kdofs.xdmf
```

This command will run shifter on 48 cores across two nodes, with the
correct libraries for MPI.

### Compiling and running natively

In order to compile and run natively, it is necessary to build a
complete software stack for FEniCS natively. This can be difficult and
time consuming, depending on the HPC system. Once installed, the
binary can be compiled with:

```
cd shifter/cpp
cmake .
make
```

and submitted with the usual batch submission script, e.g.

```
#!/bin/bash
#SBATCH --nodes=2
#SBATCH -p debug
#SBATCH -t 00:15:00
#
# Here you must set any environment needed to load FEniCS natively
# source $HOME/fenics.sh

srun -n 48 ./bench_3d --system=edison-native --filename=mesh-0048-0300Kdofs.xdmf
```

Note that the `--system` argument is only to provide a tag in the XML
which the code produces. Each run produces an XML file with timings
for various phases of the code, the filename contains a datestamp (to
prevent overwriting), and the number of cores used.

Python benchmark
----------------

As for the C++ demo, it is important to copy the MPI libraries (see
above). There is no compilation stage, except for the automatic JIT,
which takes place transparently on the first run. A sample batch
submission script is provided below.

```
#!/bin/bash
#SBATCH --image=docker:quay.io/fenicsproject:2016.1.0r1
#SBATCH --nodes=2
#SBATCH -p debug
#SBATCH -t 00:15:00
module load shifter
srun -n 48 shifter env LD_LIBRARY_PATH=$SCRATCH/cray-libs /bin/bash -c "source /home/fenics/fenics.env.conf ; python ./poisson_cube.py --system=edison-shifter --filename=mesh-0048-0300Kdofs.xdmf"
```

Running natively:

```
#!/bin/bash
#SBATCH --nodes=2
#SBATCH -p debug
#SBATCH -t 00:15:00
#
# Here you must set any environment needed to load FEniCS natively
# source $HOME/fenics.sh

srun -n 48 python ./poisson_cube.py --system=edison-native --filename=mesh-0048-0300Kdofs.xdmf
```
