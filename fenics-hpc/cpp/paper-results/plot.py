#!/usr/bin/env python

import matplotlib as mpl
mpl.use('Agg')

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import xml.etree.ElementTree as ET
import glob

acct_file = open("acct.dat","r")
acct_data = acct_file.readlines()
acct_file.close()
acct = {}
for line in acct_data:
    line = line.split()
    tm = line[2].split(':')
    tm = int(tm[0])*3600 + int(tm[1])*60 + int(tm[2])
    l0 = line[0].split('.')
    acct[l0[0]] = tm

all_times = {'System':[], 'Other':[]}
headings = ['Assemble', 'Read Mesh', 'Refine', 'Solve']
for h in headings:
    all_times[h] = []

for filename in sorted(glob.glob("N*.xml")):
    print filename
    data = ET.parse(filename)
    root = data.getroot()
    system_name = root.get("system_name")
    ncores = int(root.get("ncores"))
    if ("native" in system_name):
        system_name = "%d(a)"%ncores
    elif ("nompi" in system_name):
        system_name = "%d(c)"%ncores
    else:
        system_name = "%d(b)"%ncores
    ndofs = int(root.get("totaldofs"))
    jobid = root.get("jobid")
    print ndofs/ncores
    t0=[filename]
    all_times['System'].append(system_name)
    ttot = 0.0
    for title in headings:
        node = root.findall("./table/row[@key='BENCHMARK: %s']/col[@key='wall tot']"%title)[0]
        time = float(node.get('value'))
        print title, time
        all_times[title].append(time + ttot)
        ttot += time
    all_times['Other'].append(float(acct[jobid]))

dataframe = pd.DataFrame(all_times)
print dataframe

sns.set(style="whitegrid")

# Initialize the matplotlib figure
f, ax = plt.subplots(figsize=(12, 6))

heads=['Other']
sns.barplot(x="Other", y="System", data=dataframe,
            label="other", color='#c0c0ff', linewidth=0, errwidth=2.0)
heads = list(reversed(headings))
for h,hcol in zip(heads, ['#a0a0e0','#8080d0','#6060c0','#4040b0']):
    sns.barplot(x=h, y="System", data=dataframe,
                label=h.lower(), color=hcol, linewidth=0, ci=None)

# Add a legend and informative axis label
space = 0.3
for i, bar in enumerate(ax.patches):
    if (bar.get_y() > 2.0):
        bar.set_y(bar.get_y() + space)
    if (bar.get_y() > 5.0):
        bar.set_y(bar.get_y() + space)
    if (bar.get_y() > 8.5):
        bar.set_y(bar.get_y() + space)

for i, line in enumerate(ax.get_lines()):
    if line.get_ydata()[0] > 2.5:
        line.set_ydata(line.get_ydata() + space)
    if line.get_ydata()[0] > 5.5:
        line.set_ydata(line.get_ydata() + space)
    if line.get_ydata()[0] > 9.0:
        line.set_ydata(line.get_ydata() + space)
    
pos = []
for bar in ax.patches:
    pos.append(bar.get_y())
ax.set_yticks(np.array(pos[0:12]) + space + 0.12)
ax.set_ylim([12.5, -0.6])

handles, labels = ax.get_legend_handles_labels()

# reverse the order
ax.legend(handles[::-1], labels[::-1], ncol=1, loc="upper right", frameon=True)
ax.set(xlim=(0, 450), ylabel="number of processes",
       xlabel="time (s)")
ax.text(380, 12.05, "833.0")
ax.annotate('', xy=(445, 11.9), xytext=(400, 11.9),
            arrowprops=dict(facecolor='black', shrink=0.05, headwidth=6, width=1))
sns.despine(left=True, bottom=True)

# plt.show()
plt.savefig("cpp-shifter.pdf")
