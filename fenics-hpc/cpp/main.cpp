// Copyright (C) 2016 Chris Richardson
//
// DOLFIN is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DOLFIN is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.

// This code is intended to be a benchmark which can run scalably from
// very small to very large architectures. It reads in a file, which should
// be a unit cube mesh, and solves the Poisson equation on it.

#include <ctime>
#include <sstream>
#include <iomanip>

#include <dolfin.h>
#include <dolfin/io/XMLTable.h>
#include "pugixml.hpp"
#include "Poisson.h"

using namespace dolfin;

// Source term (right-hand side)
class Source : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  {
    double dx = x[0] - 0.5;
    double dy = x[1] - 0.5;
    values[0] = 10*exp(-(dx*dx + dy*dy)/0.02);
  }
};

// Normal derivative (Neumann boundary condition)
class dUdN : public Expression
{
  void eval(Array<double>& values, const Array<double>& x) const
  { values[0] = sin(5*x[0]); }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS; }
};

int main(int argc, char *argv[])
{
  // SCOTCH seems to be crashing
  parameters["mesh_partitioner"] = "ParMETIS";

  parameters.parse(argc, argv);

  Parameters application_parameters("application_parameters");
  application_parameters.add("filename", "mesh.xdmf");
  application_parameters.add("system", "unknown");
  application_parameters.add("nrefine", 1);
  application_parameters.add("solver", "cg");
  application_parameters.add("pc", "petsc_amg");
  application_parameters.parse(argc, argv);
  const std::string filename = application_parameters["filename"];
  const std::string solver_type = application_parameters["solver"];
  const std::string preconditioner = application_parameters["pc"];
  const std::string system_name = application_parameters["system"];

  Timer t0("BENCHMARK: Read Mesh");

  //   auto mesh = std::make_shared<Mesh>();
  std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh());
  XDMFFile file(mesh->mpi_comm(), filename);
  file.read(*mesh);
  
   //   auto mesh = std::shared_ptr<Mesh>(new UnitCubeMesh(100,100,100));

  t0.stop();
  Timer t1("BENCHMARK: Refine");
  // Refine the mesh r times
  const int r = application_parameters["nrefine"];
  for (int i = 0; i < r; ++i)
    {
      std::shared_ptr<Mesh> mesh2 = std::shared_ptr<Mesh>(new Mesh());
      refine(*mesh2, *mesh, false);
      mesh = mesh2;
    }

  t1.stop();
  Timer t2("BENCHMARK: FunctionSpace");

  auto V = std::make_shared<Poisson::FunctionSpace>(mesh);

  const int ncores = MPI::size(mesh->mpi_comm());
  if (MPI::rank(mesh->mpi_comm()) == 0)
  {
    std::cout << "Filename = " << filename << "\n";
    std::cout << "SolverType: " << solver_type << "\n";
    std::cout << "PreConditioner: " << preconditioner << "\n";
    std::cout << "Cores: " << ncores << "\n";
    std::cout << "Degrees of freedom:          " <<  V->dim() << "\n";
    std::cout << "Degrees of freedom per core: "
              <<  V->dim()/ncores << "\n";
  }

  t2.stop();
  Timer t3("BENCHMARK: DirichletBC etc");

  // Define boundary condition
  auto u0 = std::make_shared<Constant>(0.0);
  auto boundary = std::make_shared<DirichletBoundary>();
  auto bc = std::make_shared<DirichletBC>(V, u0, boundary);

  // Define variational forms
  auto a = std::make_shared<Poisson::BilinearForm>(V, V);
  auto L = std::make_shared<Poisson::LinearForm>(V);

  // Attach coefficients
  auto f = std::make_shared<Source>();
  auto g = std::make_shared<dUdN>();
  L->f = f;
  L->g = g;

  if (preconditioner == "petsc_amg")
  {
    // Apparently essential option for petsc_amg to symmetrise
    //PETScOptions::set("pc_gamg_sym_graph", true);
  }
  else if (preconditioner == "hypre_amg")
  {
    // Number of levels of aggresive coarsening for BoomerAMG (note:
    // increasing this appear to lead to substantial memory savings)
    //PETScOptions::set("pc_hypre_boomeramg_agg_nl", 4);
    //PETScOptions::set("pc_hypre_boomeramg_agg_num_paths", 2);

    // Truncation factor for interpolation (note: increasing towards 1
    // appears to reduce memory usage)
    //PETScOptions::set("pc_hypre_boomeramg_truncfactor", 0.9);

    // Max elements per row for interpolation operator
    //PETScOptions::set("pc_hypre_boomeramg_P_max", 5);

    // Strong threshold (BoomerAMG docs recommend 0.5 - 0.6 for 3D
    // Poisson)
    //PETScOptions::set("pc_hypre_boomeramg_strong_threshold", 0.5);
  }
  else if (preconditioner == "ml_amg")
  {
    // Try some options to make the smoothing more reliable for ML
    PETScOptions::set("mg_levels_ksp_chebyshev_estimate_eigenvalues",
                      "0.0,0.5,0.0,4.2");
    PETScOptions::set("mg_levels_est_ksp_type", "cg");
    PETScOptions::set("mg_levels_est_ksp_max_it", 70);
  }

  PETScOptions::set("ksp_view");

  t3.stop();
  Timer t4("BENCHMARK: Assemble");

  // Compute solution
  Function u(V);

  // Create assembler
  SystemAssembler assembler(a, L, {bc});

  // Assemble system
  std::shared_ptr<GenericMatrix> A(new PETScMatrix);
  std::shared_ptr<GenericVector> b(new PETScVector);
  assembler.assemble(*A, *b);

  // Create solver

  PETScKrylovSolver solver;

  PETScOptions::set("ksp_type", "cg");
  PETScOptions::set("pc_type", "gamg");
  // PETScOptions::set("pc_type", "hypre");
  // PETScOptions::set("pc_hypre_type", "boomeramg");
  // PETScOptions::set("pc_view");

  solver.parameters["relative_tolerance"] = 1.0e-10;
  solver.parameters["monitor_convergence"] = true;
  solver.set_operator(A);
  if (MPI::rank(mesh->mpi_comm()) == 0)
    info(solver.parameters, true);

  t4.stop();
  Timer t5("BENCHMARK: Solve");

  KSP ksp = solver.ksp();
  KSPSetFromOptions(ksp);

  // Solve
  solver.solve(*u.vector(), *b);

  t5.stop();

  // Make filename based on date for XML output of timing data
  time_t now = time(0);
  tm *ltm = localtime(&now);

  std::stringstream xml_filename;
  xml_filename << "N" << std::setw(5) << std::setfill('0') << ncores;
  xml_filename << "-" << (ltm->tm_year - 100);
  xml_filename << std::setw(2) << std::setfill('0') << (ltm->tm_mon + 1);
  xml_filename << std::setw(2) << std::setfill('0') << ltm->tm_mday;
  xml_filename << "-" << std::setw(2) << std::setfill('0') << ltm->tm_hour;
  xml_filename << std::setw(2) << std::setfill('0') << ltm->tm_min;
  xml_filename << std::setw(2) << std::setfill('0') << ltm->tm_sec << ".xml";

  // Get timing table
  //   dump_timings_to_xml(xml_filename.str(), TimingClear::clear);


  // FIXME: pugi::xml is included separately from DOLFIN here, because it is not
  // installed by DOLFIN at present. Need to be able to access nodes to add more
  // data
  pugi::xml_document doc;

  auto node = doc.append_child("bench");
  node.append_attribute("system_name") = system_name.c_str();
  node.append_attribute("ncores") = ncores;
  // FIXME: get pugi to accept 64-bit int
  node.append_attribute("totaldofs") = (int)V->dim();
  node.append_attribute("pc") = preconditioner.c_str();
  node.append_attribute("solver") = solver_type.c_str();

  // Get SLURM jobid, if available
  const char* jobid = std::getenv("SLURM_JOBID");
  if (jobid)
    node.append_attribute("jobid") = jobid;

  Table t = timings(TimingClear::clear,
    { TimingType::wall, TimingType::user, TimingType::system });

  Table t_max = MPI::max(mesh->mpi_comm(), t);
  Table t_min = MPI::min(mesh->mpi_comm(), t);
  Table t_avg = MPI::avg(mesh->mpi_comm(), t);

  XMLTable::write(t_max, node);
  //  XMLTable::write(t_min, node);
  //  XMLTable::write(t_avg, node);

  if (MPI::rank(mesh->mpi_comm()) == 0)
    doc.save_file(xml_filename.str().c_str(), "  ");

  return 0;
}
