from dolfin import *

parameters["mesh_partitioner"] = "ParMETIS"

application_parameters = Parameters("application_parameters")
application_parameters.add("filename", "mesh.xdmf")
application_parameters.add("system", "unknown")
application_parameters.add("nrefine", 1)
application_parameters.add("solver", "cg")
application_parameters.add("pc", "petsc_amg")
application_parameters.parse()

filename = application_parameters["filename"]
solver_type = application_parameters["solver"]
preconditioner = application_parameters["pc"]
system_name = application_parameters["system"]

t0 = Timer("BENCHMARK: Read Mesh")

mesh = Mesh()
file = XDMFFile(mesh.mpi_comm(), filename)
file.read(mesh)

t0.stop()

t1 = Timer("BENCHMARK: Refine")

r = application_parameters["nrefine"]
for i in range(r):
    mesh2 = refine(mesh, redistribute=False)
    mesh = mesh2

t1.stop()
t2 = Timer("BENCHMARK: FunctionSpace");

V = FunctionSpace(mesh, "CG", 1)

ncores = MPI.size(mesh.mpi_comm())
if (MPI.rank(mesh.mpi_comm()) == 0):
    print "Filename = ", filename
    print "SolverType: ", solver_type
    print "PreConditioner: ", preconditioner
    print "Cores: ", ncores
    print "Degrees of freedom:          ",  V.dim()
    print "Degrees of freedom per core: ",  V.dim()/ncores

t2.stop()
t3 = Timer("BENCHMARK: DirichletBC etc")

# Define Dirichlet boundary (x = 0 or x = 1)
def boundary(x):
    return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

f = Expression("10*exp(-(pow(x[0] - 0.5, 2) + pow(x[1] - 0.5, 2)) / 0.02)", degree=2)
g = Expression("sin(5*x[0])", degree=2)

# Define boundary condition
u0 = Constant(0.0)
bc = DirichletBC(V, u0, boundary)

# Define variational forms
u = TrialFunction(V)
v = TestFunction(V)
a = inner(grad(u), grad(v))*dx
L = f*v*dx + g*v*ds

PETScOptions.set("ksp_view")

t3.stop()
t4 = Timer("BENCHMARK: Assemble")

# Compute solution
u = Function(V)

# Create assembler
assembler = SystemAssembler(a, L, bc)

# Assemble system
A = PETScMatrix()
b = PETScVector()
assembler.assemble(A, b)

# Create solver
solver = PETScKrylovSolver("cg")

PETScOptions.set("ksp_type", "cg");
PETScOptions.set("pc_type", "hypre");
PETScOptions.set("pc_hypre_type", "boomeramg")

solver.parameters["relative_tolerance"] = 1.0e-10
solver.parameters["monitor_convergence"] = True
solver.set_operator(A);
if (MPI.rank(mesh.mpi_comm()) == 0):
    info(solver.parameters, True)

t4.stop()
t5 = Timer("BENCHMARK: Solve")

solver.set_from_options()

# Solve
solver.solve(u.vector(), b)

t5.stop()

import datetime
dt = datetime.datetime.now()
dstr="N%04d-"%ncores + dt.strftime("%y%m%d-%H%M%S") + ".xml"
dump_timings_to_xml(dstr, TimingClear_clear)

import os
import xml.etree.ElementTree as ET
if (MPI.rank(mesh.mpi_comm())==0):
    xml = ET.parse(dstr)
    root = xml.getroot()
    node_attribute={}
    if 'SLURM_JOBID' in os.environ:
        node_attribute['jobid'] = os.environ['SLURM_JOBID']
    node_attribute["system_name"] = system_name
    node_attribute["ncores"] = str(ncores)
    node_attribute["totaldofs"] = str(V.dim())
    node_attribute["pc"] = preconditioner
    node_attribute["solver"] = solver_type
    node = ET.Element("bench", node_attribute)
    root.append(node)
    xml.write(dstr)
