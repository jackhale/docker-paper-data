Containers for portable, productive and performant scientific computing; supporting data and code.
==================================================================================================

This repository contains data, code and images to support the paper:

Containers for portable, productive and performant scientific computing, Jack
S. Hale, Lizao Li, Chris Richardson, Garth N. Wells.

https://arxiv.org/abs/1608.07573

Each directory contains a `README.md` with more specific instructions.

The (Docker format) images and a snapshot of this repository are permanently
archived at http://dx.doi.org/10.6084/m9.figshare.3570813.