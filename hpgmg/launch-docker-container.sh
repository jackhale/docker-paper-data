#!/bin/bash
docker run -e HOST_GID=$(id -g) -e HOST_UID=$(id -u) \
           --rm -v $(pwd):/home/fenics/shared \
           -w="/home/fenics/shared" -ti \
           quay.io/jhale/hpgmg:docker-paper \
           "cd ~/hpgmg/build/bin; ~/shared/run-hpgmg-benchmark.sh | tee output.txt; cp output.txt ~/shared"

