#!/bin/bash
git clone https://bitbucket.org/hpgmg/hpgmg && \
cd hpgmg && \
git checkout faf66d0af5c2f8a3842052e4814ba88aa167d64d && \
./configure --fe --CFLAGS='-O2 -march=native' && \
make -j2 -C build
