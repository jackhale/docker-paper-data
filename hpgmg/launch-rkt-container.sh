#!/bin/sh
# NOTE: You will need to adjust the --dns flag for rkt to match a local DNS
# server in your environment.
sudo rkt trust --skip-fingerprint-review --prefix quay.io/fenicsproject
sudo rkt fetch quay.io/jhale/hpgmg:docker-paper
HOST_UID=$(id -u)
HOST_GID=$(id -g)
sudo rkt run --set-env=HOST_UID=$HOST_UID --set-env=HOST_GID=$HOST_GID \
	 --volume data,kind=host,source=$(pwd),readOnly=false \
	 --mount volume=data,target=/home/fenics/shared \
	 --net=host \
	 --dns=131.111.12.20 \
         --interactive quay.io/jhale/hpgmg:docker-paper \
	 --exec /sbin/my_init -- --quiet -- /sbin/setuser fenics /bin/bash -l -c 'echo "127.0.0.1 $(hostname)" | sudo tee --append /etc/hosts; cd ~/hpgmg/build/bin; ~/shared/run-hpgmg-benchmark.sh | tee output.txt; cp output.txt ~/shared'
