#!/usr/bin/env python
import numpy as np
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd

def postprocess_hpgmg_ascii(filename):
    df = pd.DataFrame(columns=["X", "Y", "Z", "t (s)", "Gigaflops", "MEquations/s"])
    with open(filename) as f:
        docker_raw = f.read()

    results_begun = False
    for i, line in enumerate(docker_raw.splitlines()):
        if line == "Starting performance sampling":
            results_begun = True
            continue
        if results_begun == True:
            ss = line.split()
            result = {"X": int(ss[2]), "Y": int(ss[3]), "Z": int(ss[4][:-1]),
                      "t (s)": float(ss[9]), "Gigaflops": float(ss[11]),
                      "MEquations/s": float(ss[13])}
            result = pd.Series(result)
            df = df.append(result, ignore_index=True)

    for coord in ["X", "Y", "Z"]:
        df[coord] = df[coord].astype(int)

    return df

native = postprocess_hpgmg_ascii('edison-native-192.txt')
native["system"] = "native"
docker = postprocess_hpgmg_ascii('edison-shifter-192.txt')
docker["system"] = "Shifter"
df = pd.concat([docker, native])
df["Test"] = df.apply(lambda row: str(row["X"]) + "x" + str(row["Y"]) + "x" + str(row["Z"]), axis=1)

sns.set_style("whitegrid")
f, ax = plt.subplots(figsize=(12, 4))

pal = sns.color_palette('Greens_d',2)
sns.barplot(x=df["Gigaflops"], y=df["Test"], hue=df.system, linewidth=0, palette=pal, errwidth=2.0)
plt.ylabel("grid size")
plt.xlabel("gigaflops")
matplotlib.pyplot.subplots_adjust(bottom=0.15)
plt.legend(ncol=1, frameon=True)
sns.despine(left=True, bottom=True)

plt.savefig('hpgmg-edison.pdf')
