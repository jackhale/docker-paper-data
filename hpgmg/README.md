HPGMG on Workstation
====================

We assume that you have working installations of Docker and/or rkt available.

Before beginning, please modify the `-n` flag to the `mpiexec` command
in the file `run-hpgmg-benchmark.sh` to the number of MPI processes you wish
to use (typically one per core).

We compiled HPGMG using the script:

    ./build-hpgmg.sh

This requires a working install of PETSc to be in your path.

Once you have a working install of HPGMG installed on the host system then run:

    cd hpgmg/build/bin
    ../../../run-hpgmg-benchmark.sh | tee ../../../output.txt

To automatically run the test inside a Docker container:

    ./launch-docker-container.sh

To automatically run the test inside a rkt container:

    ./launch-rkt-container.sh

All of the above commands output the results to the file 'output.txt' in this
directory.

HPGMG on HPC
============
Compiling and running HPGMG on Edison is done in the same way as for
the C++ benchmark, see the "shifter" folder in this
repository. Briefly, we first compile in a container by launching an
interactive session:

```
salloc -N 1 -p debug -t 0:30:0 --image=quay.io/fenicsproject/latest:2016.1.0r1
shifter /bin/bash
source build-hpgmg.sh
```

and to run, we must replace the container MPI with the Cray MPI:

```
#!/bin/bash
#SBATCH --image=docker:quay.io/fenicsproject:2016.1.0r1
#SBATCH --nodes=2
#SBATCH -p debug
#SBATCH -t 00:15:00

module load shifter
srun -n 48 shifter env LD_LIBRARY_PATH=$SCRATCH/cray-libs ./hpgmg-fe
```

Note that it is important to check the `RPATH` and `RUNPATH` of the
`hpgmg-fe` binary, as they may override the values provided by
`LD_LIBRARY_PATH`, in which case it may be necessary to use `chrpath`
or another tool to enforce the library loading order.

For the native compile, the appropriate PETSc modules must be
available, and then `source build-hpgmg.sh` can be run to compile the
code, and run in the usual way with the system batch scripts.

Raw data
========

The raw data to generate the figures in the paper is
located in the directory `paper-results/`. To generate both plots
automatically using Docker, run:

    cd paper-results/
    ./plot-galah.sh
    ./plot-edison.sh
