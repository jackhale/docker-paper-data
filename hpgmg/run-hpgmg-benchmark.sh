#!/bin/bash
# NOTE: Adjust the -n flag to suit your CPU setup.
mpiexec -n 16 ./hpgmg-fe sample -local 50,1000000 -op_type poisson2
