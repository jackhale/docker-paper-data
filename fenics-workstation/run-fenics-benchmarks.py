#!/usr/bin/env python
from dolfin import *
from os import listdir
from os.path import isfile, join
import timeit
import pandas as pd

custom_import_statement = """
from dolfin import UnitSquareMesh, FunctionSpace, Constant, DirichletBC, TrialFunction, TestFunction, Expression, inner, grad, dx, ds, Function, solve, DOLFIN_EPS, File, plot, Mesh, MeshFunction, VectorFunctionSpace, TrialFunctions, TestFunctions, div, parameters, UnitCubeMesh, CompiledSubDomain, pi, Identity, tr, det, ln, dot, derivative, mpi_comm_world, has_linear_algebra_backend, MPI, interpolate, MPI, HDF5File, set_log_level, PROGRESS, Timer, info, refine, PETScOptions, assemble, LinearSolver, sym, assemble_system, VectorSpaceBasis, PETScPreconditioner, PETScKrylovSolver, as_backend_type, XDMFFile, VectorElement, FiniteElement, MixedElement, project, adapt
"""

def generate_refined_mesh():
    if isfile("python/pulley_refined.xdmf"): 
        print "Refined mesh already generated, skipping..."
        return None
     
    print "Refining pulley.xml.gz..."
    mesh = Mesh("python/pulley.xml.gz")

    for i in range(2):
        print "Refinement %d..." % i
        mesh = refine(mesh)
    
    print "Writing to pulley_refined.xdmf..."
    XDMFFile("python/pulley_refined.xdmf").write(mesh)

def benchmark():
    mode = "final"
    path = "python/"
    
    if mode == "debug":
        params = {"io_test.py": {"repeat": 1, "number": 1}}
    elif mode == "poster":
	params = {"demo_elasticity.py": {"repeat": 5, "number": 6},
                  "demo_poisson.py": {"repeat": 5, "number": 80},
                  "poisson_boomer.py": {"repeat": 5, "number": 1},
                  "io_test.py": {"repeat": 5, "number": 1}}
    elif mode == "final":
    	# custom params for each timeit run on each benchmark file
        params = {"demo_poisson.py": {"repeat": 5, "number": 3},
		  "demo_elasticity.py": {"repeat": 5, "number": 3},
		  "poisson_boomer.py": {"repeat": 5, "number": 1},
		  "io_test.py": {"repeat": 5, "number": 5}}
    
    results = pd.DataFrame(columns=["test", "time"])
    # loop over benchmark files with timeit
    for f in params:
        f_handle = open(join(path, f), 'r')
        code = f_handle.read()
        try:
            repeat = params[f]['repeat']
            number = params[f]['number']
        except KeyError:
            print "No default repeat or number setting for %s, using defaults." % f
            repeat = 1 
            number = 1
        print "Running benchmark %s with repeat=%d and number=%d" % (f, repeat, number)
        ts = timeit.repeat(code, setup=custom_import_statement, 
                           repeat=repeat, number=number)
        for t in ts:
	    result = pd.Series({"test": f, "time": t})
            results = results.append(result, ignore_index=True)

        if len(ts) != 0:
            print "%s: %.4f" % (f, min(ts))

    # need to output results here for analysis in seperate file
    print results
    results.to_json('end-user-benchmark.json')   

if __name__ == "__main__":
    generate_refined_mesh()
    benchmark() 
