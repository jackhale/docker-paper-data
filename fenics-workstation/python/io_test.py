from dolfin import *
import shutil
import os
# In docker container we might choose to write to shared folder
# or folder on the layered filesystem
output_directory = "python/output/"

if MPI.rank(mpi_comm_world()) == 0: 
	# Clean output from previous test
	shutil.rmtree(output_directory, ignore_errors=True)
	# Make new directory if needed
	if not os.path.exists(output_directory): os.mkdir(output_directory)

# Read xdmf
f = XDMFFile("python/pulley_refined.xdmf")
mesh = Mesh()
f.read(mesh)

# Interpolate function and write to hdf5
V = VectorFunctionSpace(mesh, "CG", 1)
f_e = Expression(("3.0", "3.0", "3.0"))
f_V = interpolate(f_e, V)

# Write out to hdf5
h5 = HDF5File(mpi_comm_world(), output_directory + "output.h5", "w")
h5.write(f_V, "f_V")
h5.close()
