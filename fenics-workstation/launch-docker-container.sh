#!/bin/bash
docker run -e HOST_GID=$(id -g) -e HOST_UID=$(id -u) \
           --rm -v $(pwd):/home/fenics/shared \
           -w="/home/fenics/shared" -ti \
           quay.io/fenicsproject/stable:docker-paper \
           "pip install --user pandas==0.18.1; python run-fenics-benchmarks.py; python run-fenics-benchmarks.py" 
