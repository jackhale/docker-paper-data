FEniCS on Workstation
=====================

We assume that you have working installations of Docker and/or rkt available.

If you have FEniCS 2016.1.0 and pandas installed on the host system the tests
can be run with the file:

    ./run-fenics-benchmarks.py

Remember to run twice to prime the JIT object cache to get consistent timings.

To automatically run the tests twice inside a Docker container:

    ./launch-docker-container.sh

To automatically run the tests twice inside a rkt container:

    ./launch-rkt-container.sh

You will need to adjust the `--dns` flag in the script `./run-rkt-container.sh`
before running the above command.

All of the above commands output the results to the file
`end-user-benchmark.json` in this directory.

Raw data
========

The raw data from the above scripts used to generate the figure in the paper is
located in the directory `paper-results/`. Information about the galah machine
we used can be found in the directory `paper-results/galah-information`. To
generate a plot of our results automatically using Docker, run:

    cd paper-results/
    ./plot.sh
