#!/bin/bash
# NOTE: You will need to adjust the --dns flag for rkt to match a local DNS in
# your environment.
sudo rkt trust --skip-fingerprint-review --prefix quay.io/fenicsproject
sudo rkt fetch quay.io/fenicsproject/stable:docker-paper
HOST_UID=$(id -u)
HOST_GID=$(id -g)
sudo rkt run --set-env=HOST_UID=$HOST_UID --set-env=HOST_GID=$HOST_GID \
	 --volume data,kind=host,source=$(pwd),readOnly=false \
	 --mount volume=data,target=/home/fenics/shared \
	 --dns=131.111.12.20 \
	 --interactive \
     quay.io/fenicsproject/stable:docker-paper \
     --exec /sbin/my_init -- --quiet -- /sbin/setuser fenics /bin/bash -l -c "pip install --user pandas==0.18.1; cd ~/shared; python run-fenics-benchmarks.py; python run-fenics-benchmarks.py"
