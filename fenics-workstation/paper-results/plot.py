#!/usr/bin/env python
import numpy as np
import matplotlib
matplotlib.use('Agg')
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

docker = pd.read_json('end-user-benchmark-docker.json')
rkt = pd.read_json('end-user-benchmark-rkt.json')
native = pd.read_json('end-user-benchmark-native.json')
vm = pd.read_json('end-user-benchmark-vm.json')

rkt['system'] = "rkt"
docker['system'] = "Docker"
vm['system'] = "VM"
native['system'] = "native"
df = pd.concat([docker, rkt, native, vm])

# add some prettier titles
df['test_pretty'] = ""
df.loc[df['test'] == 'demo_elasticity.py', 'test_pretty'] = "elasticity"
df.loc[df['test'] == 'io_test.py', 'test_pretty'] = "I/O"
df.loc[df['test'] == 'poisson_boomer.py', 'test_pretty'] = "Poisson AMG"
df.loc[df['test'] == 'demo_poisson.py', 'test_pretty'] = "Poisson LU"

sns.set_style("whitegrid")
f, ax = plt.subplots(figsize=(12, 4))

pal = sns.color_palette("Blues_d", 4)
sns.barplot(x=df.time, y=df.test_pretty, hue=df.system, linewidth=0, palette=pal, errwidth=2.0)
plt.xlabel('time (s)')
plt.ylabel('test type')
plt.legend(loc='lower right', ncol=1, frameon=True)
matplotlib.pyplot.subplots_adjust(bottom=0.15)
sns.despine(left=True, bottom=True)

plt.savefig('end-user-benchmark.pdf')
